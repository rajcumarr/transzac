<?php
    global $user;
    $default_value = theme_get_setting('logo_normal_file_uploaded');
    $path = base_path() . drupal_get_path("theme","md_delphinus") . "/logo.png";
    $media_file = array('fid' => isset($default_value['fid']) ? intval($default_value['fid']) : 0);
    if ($media_file['fid'] && ($media_file = file_load($media_file['fid']))) :
      $media_file = get_object_vars($media_file);
    endif;
    
    if($media_file['fid'] != 0) :
        $path = file_create_url($media_file['uri']);
    endif;
?>
<?php
if (module_exists('commerce')) :
    global $user;
    $quantity = 0;
    $order = commerce_cart_order_load($user->uid);
    if ($order) :
        $wrapper = entity_metadata_wrapper('commerce_order', $order);
        $line_items = $wrapper->commerce_line_items;
        $quantity = commerce_line_items_quantity($line_items, commerce_product_line_item_types());
        $total = commerce_line_items_total($line_items);
        $currency = commerce_currency_load($total['currency_code']);
    endif;
endif;
?>
<!-- header-full-center -->
<header id="header" class="header-shadow header-full-center">
    <div class = 'head sticky-header'>
        <div class="topbar tophead">
            <div class="row">
                <div class="topbar-left col-sm-3 icon-z-up">
                    <div class="branding icon-pad-up" >
                        <h1 class="logo">
                            <a href="<?php print base_path(); ?>"><img src="<?php print $path; ?>" alt="" /></a>
                        </h1>
                    </div><!-- .branding -->
                </div><!-- .topbar-left -->
                <div class="col-sm-5 search-pad-up" >
                    <ul id="main-nav-tool">
                        <li class="search-action">
                        <?php
                           @print drupal_render(drupal_get_form('search_block_form')); 
                        ?>
                         </li>

                    </ul><!-- #main-nav-tool --> 
                </div>        
                <div class="topbar-right col-sm-4">
                    <ul class="top-navigation">
                        <?php 
                            if (!user_is_logged_in()) :
                        ?>
                                <li class="myaccount-item">
                                    <a href="javascript:void(0)"><?php print t('Login') ?></a>
                                    <div class="top-navigation-submenu">
                                        <h3 class="submenu-heading"><?php print t('Login') ?></h3>
                                        <?php
                                          $block = module_invoke("user", "block_view", "login");
                                          print render($block['content']);
                                        ?>
                                    </div>
                                </li>
                        <?php
                            endif;
                        ?>
                        <?php
                            $wish_block = module_invoke("views", "block_view", "product-wishlist_block");
                            if(!empty($wish_block['content'])) :
                        ?>
                        <li class="wishlist-item">
                            <?php
                              $block = module_invoke("views", "block_view", "product-wishlist_block");
                              print render($block['content']);
                            ?>
                        </li>
                        <?php
                            endif;
                        ?>
                        <?php 
                            if(!user_is_logged_in()) : 
                        ?>
                        <li class="wishlist-item">
                            <a href='/seller/register'>Sell on Transzac</a>
                        </li>
                        <li class="wishlist-item">
                            <a href='/buyer/register'>Register</a>
                        </li>
                        <li class="shopping-bag-item">
                        <a href="<?php print base_path() ?>cart"><?php print t('Cart') ?> <span><?php print format_plural($quantity, '1', '@count'); ?></span></a>
                        <div class="top-navigation-submenu">
                            <div class="shopping-bag">
                                <?php
                                  $block = module_invoke("commerce_cart", "block_view", "cart");
                                  print render($block['content']);
                                ?>
                            </div>
                        </div>

                    </li>
                        <?php
                            endif;
                        ?>        
                        <?php
                            if (user_is_logged_in()) :
                        ?>
                        <li>
                            <span>
                                <a class="" href="<?php print base_path() ?>">
                                    <i class="fa fa fa-home fa-2x" title="Home"></i><br/>
                                    <p style="text-align: center;">Home</p>
                                </a>
                            </span>
                        </li>
                        <li>
                            <span>
                                <a class="" href="<?php print base_path() ?>user/<?php print $user->uid; ?>/my-shop">
                                    <i class="fa fa fa-shopping-bag fa-2x" title="Your Shop"></i><br/>
                                    <p style="text-align: center;">Your Shop</p>
                                </a>
                            </span>
                        </li>
                        <li >
                            <span>
                                <a class="" href="<?php print base_path() ?>cart">

                                    <span class="count_label btn-metis-4">
                                        <?php print format_plural($quantity, '1', '@count'); ?>
                                    </span>
                                        <i class="fa fa fa-shopping-cart fa-2x" title="Cart"></i><br/>
                                        <p style="text-align: center;">My Cart</p>
                                </a>
                            </span>
                            <div class="top-navigation-submenu">
                                <div class="shopping-bag">
                                    <?php
                                      $block = module_invoke("commerce_cart", "block_view", "cart");
                                      print render($block['content']);
                                    ?>
                                </div>
                            </div>
                        </li>
                        <li >
                            <span>
                                <a class="" href="<?php print base_path() ?>user">
                                <?php
                                    $user_image = theme("user_picture",array('account' =>$user));
                                    $user_image = strlen($user_image)?$user_image:'<a href = "user/' . $user->uid . '"><img src = "https://cdn2.iconfinder.com/data/icons/rcons-user/32/male-shadow-fill-circle-512.png"alt="usr_profile"></a>';
                                ?>
                                    <!-- <i class="fa fa-user fa-2x" title="You"></i><br/> -->
                                    <div class="user_icon">
                                        <?=$user_image; ?>
                                        <p class='you'>You</p>
                                    </div>
                                </a>
                            </span>
                            <div class="top-navigation-submenu user_menu_list">
                                <div class="user_menu">
                                    <?php
                
                                        $str = <<<EOD

                                            <div class='user'>
                                                <div class = 'user_image'>
                                                    <ul>
                                                        <li>
                                                            {$user_image}
                                                        </li>
                                                    </ul>
                                                    <div class = 'user_name'>
                                                        <p class="name"> {$user->name} </p>

                                                    </div>
                                                </div>
                                            </div>

EOD;
                                        print $str;        
                                        $menu = menu_navigation_links('user-menu');
                                        print theme('links__menu-external-links', array('links' => $menu));
                                    ?>
                                </div>
                            </div>
                        </li>
                        <?php
                            endif;
                        ?>
                        
                    </ul><!-- .top-navigation -->
                </div><!-- .topbar-right -->

            </div>
        </div><!-- .topbar -->
    </div>
    <div class="navbar-container">
        <div class="navbar-container-inner clearfix">
            <!-- <div class="branding">
                <h1 class="logo">
                    <a href="<?php print base_path(); ?>"><img src="<?php print $path; ?>" alt="" /></a>
                </h1>
            </div><!-- .branding --> 
    
            <div class="mobile-tool">
                <a id="hamburger-icon" href="#" title="Menu">
                    <span class="hamburger-icon-inner">
                        <span class="line line-1"></span>
                        <span class="line line-2"></span>
                        <span class="line line-3"></span>
                    </span>
                </a>
                <a href="<?php print base_path() . 'cart'; ?>" class="mobile-tool-cart">
                    <i class="icon-Shopping-Cart"></i>
                </a>
            </div>
            
        </div>
    </div><!-- .navbar-container -->
    <nav class="main-nav-mobile" id="nav-mobile">
        <?php
            $menu_name = variable_get('menu_main_links_source', 'main-menu');
            $tree = menu_tree($menu_name);
            print str_replace('id="main-navigation"','class="navigation-mobile"',drupal_render($tree)); 
        ?>
    </nav><!-- .main-nav-mobile -->
</header><!-- #header -->