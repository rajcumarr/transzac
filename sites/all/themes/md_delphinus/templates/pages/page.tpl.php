

<div id="search-fullwidth" class="mfp-hide mfp-with-anim">
    <?php
		$block = module_invoke('search', 'block_view', 'form');
		print render($block['content']);
	?>
</div><!-- #search-fullwidth -->

<?php if(isset($awecontent)) : ?>
<div id="page-wrapper">
    <div id="page">
	   
       <?php if($page['header']):?>
          <?php print render($page['header']);?>
       <?php endif; ?>
	   
	   <?php if($page['banner']):?>
          <div class="page-section no-padding" >
              <?php print render($page['banner']);?>
          </div><!-- .page-section -->
       <?php endif; ?>
       <div id="main">
          <?php if($messages) : ?>
            <div class="page-section">
              <div class="container">
                <?php print $messages; ?>
              </div>
            </div>
          <?php endif; ?>
          
          <?php if ($tabs): ?><div class="config-tool"><?php print render($tabs); ?></div><?php endif; ?>
          <?php print render($page['content']);?>          
       </div><!-- #main -->
       
       <?php if (!isset($_GET['ac_layout'])) : ?>
           <div id="footer">
           
              <?php if($page['footer_top']):?>
                <div id="footer-top">
                    <div class="container">
                        <?php print render($page['footer_top']);?>
                    </div>
                </div>
              <?php endif; ?>
        
              <?php if($page['footer']):?>
                <?php print render($page['footer']);?>
              <?php endif; ?>
           </div><!-- #footer -->
      <?php endif; ?>
    
    </div><!-- #page -->
</div>
<?php if (!isset($_GET['ac_layout'])) : ?>
	<script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery("#rev_slider_1").show().revolution({
                sliderType:"carousel",
                sliderLayout:"fullwidth",
                dottedOverlay:"none",
                delay:1000,
                carousel: {
                    maxRotation: 0,
                    minScale: 65,
                    maxVisibleItems: 3,
                    infinity: "on",
                    space: -50,
                    vary_fade: "on",
                    stretch: "off"
                },
                gridwidth:450,
                gridheight: 540,
                stopAfterLoops:0,
                stopAtSlide:1,
                disableProgressBar:"on"
            });
        }); /*ready*/
    
    </script>
<?php endif; ?>
<?php else : ?>
<div id="page-wrapper">
    <div id="page">
		   
		   <?php if($page['header']):?>
              <?php print render($page['header']);?>
           <?php endif; ?>
           <?php if($page['banner']):?>
           	  <div class="page-section no-padding" >
				  <?php print render($page['banner']);?>
              </div><!-- .page-section -->
           <?php endif; ?>
           <div id="main">
                <div class="page-section">
                  <div class="container">
                    <?php if($messages) : ?>
						<?php print $messages; ?>
                    <?php endif; ?>
                    <?php if (!empty($page['sidebar'])) : ?>
						<?php if(theme_get_setting('sidebar_position') == 'left') : ?>
                          <div class="col-md-3 col-sm-3">
                            <aside class="side-bar">
                              <?php print render($page['sidebar']); ?>
                            </aside>
                          </div>
                        <?php endif; ?>
                        <?php if(theme_get_setting('sidebar_position') == 'no') : ?>
                          <div class="col-md-12 col-sm-12">
                            <?php print $messages; ?>
                            <?php if ($tabs): ?><div class="tab-tool"><?php print render($tabs); ?></div><?php endif; ?>
                            <?php print render($page['content']); ?>
                          </div>
                        <?php else: ?>
                          <div class="col-md-9 col-sm-9">
                            <?php print $messages; ?>
                            <?php if ($tabs): ?><div class="tab-tool"><?php print render($tabs); ?></div><?php endif; ?>
                            <?php print render($page['content']); ?>
                          </div>
                        <?php endif; ?>
                        <?php if(theme_get_setting('sidebar_position') == 'right' || theme_get_setting('sidebar_position') == "") : ?>
                          <div class="col-md-3 col-sm-3">
                            <aside class="side-bar">
                              <?php print render($page['sidebar']); ?>
                            </aside>
                          </div>
                        <?php endif; ?>
                        <?php else: ?>
                          <div class="col-md-12 col-sm-12">
                              <?php print $messages; ?>
                              <?php if ($tabs): ?><div class="tab-tool"><?php print render($tabs); ?></div><?php endif; ?>
                              <?php print render($page['content']);?>
                          </div>
                        <?php endif; ?>
                  </div>
                </div>
              
           </div><!-- #main -->
    
    
           <div id="footer">
       
			  <?php if($page['footer_top']):?>
                <div id="footer-top">
                    <div class="container">
                        <?php print render($page['footer_top']);?>
                    </div>
                </div>
              <?php endif; ?>
        
              <?php if($page['footer']):?>
                <?php print render($page['footer']);?>
              <?php endif; ?>
           </div><!-- #footer -->
    
    </div><!-- #page -->
</div>
<?php endif; ?>