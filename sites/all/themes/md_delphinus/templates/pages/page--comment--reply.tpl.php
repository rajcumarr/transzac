<div id="search-fullwidth" class="mfp-hide mfp-with-anim">
    <?php
		$block = module_invoke('search', 'block_view', 'form');
		print render($block['content']);
	?>
</div><!-- #search-fullwidth -->

<div id="page-wrapper">
    <div id="page">
           <?php if($page['header']):?>
              <?php print render($page['header']);?>
           <?php endif; ?>
           <?php if($page['banner']):?>
           	  <div class="page-section no-padding" >
				  <?php print render($page['banner']);?>
              </div><!-- .page-section -->
           <?php endif; ?>
           <div id="main">
              
              
              <div class="page-section">
                <div class="container">
				  <?php if($messages) : ?>
                  	<?php print $messages; ?>
                  <?php endif; ?>
                  <?php print render($tabs); ?>
				  <?php print render($page['content']);?>
                </div>
              </div>
              
              
           </div><!-- #main -->
    
    
           <div id="footer">
       
			  <?php if($page['footer_top']):?>
                <div class="container">
                    <?php print render($page['footer_top']);?>
                </div>
              <?php endif; ?>
        
              <?php if($page['footer']):?>
                <?php print render($page['footer']);?>
              <?php endif; ?>
           </div><!-- #footer -->
    
    </div><!-- #page -->
</div>