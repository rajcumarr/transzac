<div class="page-section">
    <div class="product-main style1">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">

                    <div class="product-detail-thumbarea">
                        <div class="single-product-main-images owl-carousel1" id="sync1">
                            <?php for($i=0; $i < count($content['field_product_gallery']['#items']); $i++) : ?>
                                <div class="easyzoom easyzoom--overlay">
                                    <a href="<?php print file_create_url($content['field_product_gallery']['#items'][$i]['uri']) ?>" class="woocommerce-main-image">
                                        <img alt="" src="<?php print file_create_url($content['field_product_gallery']['#items'][$i]['uri']) ?>" />
                                    </a>
                                </div>
                            <?php endfor; ?>
                        </div><!-- #sync1.single-product-main-images.owl-carousel -->
                        <div class="single-product-main-thumbnails owl-carousel2" id="sync2" data-items="3">
                            <?php for($i=0; $i < count($content['field_product_gallery']['#items']); $i++) : ?>
                                <a href="<?php print file_create_url($content['field_product_gallery']['#items'][$i]['uri']) ?>" class="woocommerce-main-image">
                                    <img alt="" src="<?php print image_style_url('product_4', $content['field_product_gallery']['#items'][$i]['uri']) ?>" >
                                </a>
                            <?php endfor; ?>
                        </div><!-- #sync2.single-product-main-thumbnails.owl-carousel -->
                    </div><!-- .product-detail-thumbarea -->

                </div>
                <div class="col-sm-6">
                    <?php
                        $block = module_invoke('md_theme', 'block_view', 'breadcrumbs_2');
                        print render($block['content']);
                    ?>
                    <h1 class="product-title"><?php print $node->title; ?></h1>
                    <div class="product-price-wrap clearfix">
                        <div>
                            <h3 class="price"><span class="amount"><?php print render($content['product:commerce_price']) ?></span></h3>
                        </div>
                        <div class="review-summary">
                            <div class="star-rating">
                                <?php print render($content['field_product_rating']); ?>
                            </div>
                        </div>
                    </div>

                    <div class="product-details-info">
                        <?php print render($content['field_product_ref']); ?>
                        <div class="cart">
                            <div class="cart-tool clearfix">
                                <a class="add_to_wishlist" href="#" data-toggle="tooltip" data-placement="top" title="Add to wishlist">
                                    <i class="fa fa-heart"></i>
                                </a>
                                <a class="add_to_compare" href="#" data-toggle="tooltip" data-placement="top" title="Compare">
                                    <i class="fa fa-exchange"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="product-short">
                        <p><?php print $content['body']['#items'][0]['summary']; ?></p>
                        <p>
                            <strong class="black"><?php print t('Features and materials') ?></strong>
                            <?php for($i=0; $i < count($content['field_product_features']['#items']); $i++) : ?>
                                <br/><?php print $content['field_product_features']['#items'][$i]['value'] ?>
                            <?php endfor; ?>
                        </p>
                        <p><strong class="black"><?php print t('Dimensions') ?></strong><br/><?php print $content['field_product_dimensions']['#items'][0]['value'] ?></p>
                    </div>

                    <div class="product_meta">
                        <span class="sku_wrapper"><?php print t('SKU') ?>: <span class="sku"><?php print $content['product:sku']['#sku']; ?></span></span>
                        <span class="posted_in">
                            <?php print t('Category') ?>: 
                            <?php for($i=0; $i < count($content['field_product_category']['#items']); $i++) : ?>
                              <?php if($i < count($content['field_product_category']['#items']) - 1) : ?>
                                  <a href="<?php print base_path() . drupal_get_path_alias($content['field_product_category'][$i]['#href']) ?>"><?php print $content['field_product_category'][$i]['#title']; ?></a>, 
                              <?php else : ?>
                                  <a href="<?php print base_path() . drupal_get_path_alias($content['field_product_category'][$i]['#href']) ?>"><?php print $content['field_product_category'][$i]['#title']; ?></a>
                              <?php endif; ?>
                            <?php endfor; ?>
                        </span>
                    </div>

                    <div class="product-shareit">
                        <span class="screen-reader-text"><?php print t('Share this') ?></span>
                        <ul class="social_icons clearfix">
                            <li><a onclick="window.open('https://www.facebook.com/sharer.php?s=100&amp;p[url]=<?php print drupal_get_path_alias('node/' . $node->nid); ?>','sharer', 'toolbar=0,status=0,width=620,height=280');" href="javascript:;"><i class="fa fa-facebook"></i> <span><?php print t('Facebook'); ?></span></a></li>
                            <li><a onclick="popUp=window.open('http://twitter.com/home?status=<?php print str_replace(" ","+",$node->title);?> <?php print drupal_get_path_alias('node/' . $node->nid); ?>','sharer','scrollbars=yes,width=800,height=400');popUp.focus();return false;" href="javascript:;"><i class="fa fa-twitter"></i> <span><?php print t('Twitter'); ?></span></a></li>
                            <li><a onclick="popUp=window.open('http://pinterest.com/pin/create/button/?url=<?php print drupal_get_path_alias('node/' . $node->nid); ?>&amp;description=<?php print str_replace(" ","+",$node->title);?>&amp;media=<?php print file_create_url($content['field_product_thumbnail']['#items'][0]['uri']);?>','sharer','scrollbars=yes,width=800,height=400');popUp.focus();return false;" href="javascript:;"><i class="fa fa-pinterest"></i> <span><?php print t('Pinterest'); ?></span></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="page-section bg-gray">
    <div class="container">
        <div class="woocommerce-tabs">
            <!-- Nav tabs -->
            <ul class="nav clearfix" role="tablist">
                <li role="presentation" class="active"><a href="#tab-description_tab" role="tab" data-toggle="tab"><?php print t('Description'); ?></a></li>
                <li role="presentation"><a href="#tab-additional_information" role="tab" data-toggle="tab"><?php print t('Additional INFO'); ?></a></li>
                <li role="presentation"><a href="#tab-reviews" role="tab" data-toggle="tab"><?php print t('Review'); ?></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div id="tab-description_tab" role="tabpanel" class="tab-pane active">
                    <div class="row">
                        <div class="col-md-12">
                            <?php print $content['body']['#items'][0]['value'] ?>
                        </div>
                    </div>
                </div>
                <div id="tab-additional_information" role="tabpanel" class="tab-pane">
                    <?php print $content['field_product_additional_info']['#items'][0]['value'] ?>
                </div>
                <div id="tab-reviews" role="tabpanel" class="tab-pane">
                    <?php print render($content['comments']); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="page-section">
        <div class="container">

            <div class="kt-heading-wrapper">
                <div class="kt-heading-divider">
                    <svg version="1.1" x="0px" y="0px"
                             viewBox="349 274.7 1310.8 245.3" style="enable-background:new 349 274.7 1310.8 245.3;" xml:space="preserve">
                        <path d="M1222,438.9c-2.7,0-5.4,0-8.1-2.7l-210.8-129.7L792.3,436.2c-5.4,2.7-10.8,2.7-13.5,0L573.3,306.5L365.2,436.2L349,411.9
                            l216.2-132.4c5.4-2.7,10.8-2.7,13.5,0l208.1,127l210.8-129.7c5.4-2.7,10.8-2.7,13.5,0L1222,409.2l208.1-129.7
                            c5.4-2.7,10.8-2.7,13.5,0l216.2,135.1l-13.5,21.7l-208.1-129.7l-208.1,129.7C1227.4,436.2,1224.7,438.9,1222,438.9L1222,438.9z"/>
                            <path d="M1222,520c-2.7,0-5.4,0-8.1-2.7l-210.8-129.7L792.3,517.3c-5.4,2.7-10.8,2.7-13.5,0L573.3,387.6L362.5,517.3L349,493
                            l216.2-132.4c5.4-2.7,10.8-2.7,13.5,0l205.4,129.7L995,360.5c5.4-2.7,10.8-2.7,13.5,0l210.8,129.7l208.1-129.7
                            c5.4-2.7,10.8-2.7,13.5,0l216.2,135.1l-13.5,21.6l-205.4-129.7l-208.1,129.8C1227.4,517.3,1224.7,520,1222,520L1222,520z"/>
                        </svg>
                </div>
                <h3 class="kt-heading-title"><?php print t('LATEST PRODUCT') ?></h3>
            </div>
            
            <?php
                $block = module_invoke('views', 'block_view', 'product-product_slider_2');
                print render($block['content']);
            ?>
       </div>
</div>
<?php
    $path_theme = drupal_get_path('theme', 'md_delphinus');
    drupal_add_js("{$path_theme}/js/front/jquery-ui.min.js");
?>